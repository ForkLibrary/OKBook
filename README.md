# OK小说 
开源地址：[https://gitee.com/xcode_xiao/OKBook](https://gitee.com/xcode_xiao/OKBook)

**APK文件下载：[传送门](https://fir.im/OkBook)**
> kotlin + 协程 + MVVM 模式来编写的看小说APP。

## 主要框架
- **Lifecycle**  [传送门](https://developer.android.com/topic/libraries/architecture/lifecycle)
- **ViewModel** [传送门](https://developer.android.com/topic/libraries/architecture/viewmodel)
- **LiveData**  [传送门](https://developer.android.com/topic/libraries/architecture/livedata)
- **Kotlin + 协程** [传送门](https://github.com/Kotlin/kotlinx.coroutines)
## 网络框架
- **OKhttp**  [传送门](https://github.com/square/okhttp)
- **Retrofit** [传送门](https://github.com/square/retrofit)
## 缓存框架
- **OkHttpUtil** [传送门](https://gitee.com/xcode_xiao/OkHttpHelper)
## 解析框架
- **Jsoup** [传送门](https://jsoup.org/)

### 图片
![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/1.jpg)

----------------

![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/2.jpg)

----------------

![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/3.jpg)

----------------

![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/4.jpg)

----------------

![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/5.jpg)

----------------

![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/6.jpg)

----------------

![image](https://gitee.com/xcode_xiao/OKBook/raw/master/app/src/main/assets/7.jpg)

----------------


QQ群：[Kotlin/Android开发 : 559259945](https://jq.qq.com/?_wv=1027&k=52jzweZ)

> The End
